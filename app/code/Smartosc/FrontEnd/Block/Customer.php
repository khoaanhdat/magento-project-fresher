<?php
namespace Smartosc\FrontEnd\Block;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Customer\Model\SessionFactory;

class Customer extends Template
{
	protected $_customerSession;

	public function __construct(
		Context $context,
		SessionFactory $customerSession,
		array $data = []
	) {
		$this->_customerSession = $customerSession;
		parent::__construct($context, $data);
	}

	public function getCustomerFirstName() {
		if ($this->_customerSession->create()->isLoggedIn()) {
			return $this->_customerSession->create()->getCustomer()->getFirstname();
		}
		return false;
	}
}