<?php
namespace Smartosc\FrontEnd\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;

class Search extends Action
{
	/**
	 * @var CollectionFactory
	 */
	protected $_productCollectionFactory;

	/**
	 * @var JsonFactory
	 */
	protected $_resultJsonFactory;


	public function __construct(Context $context,
								JsonFactory $resultJsonFactory,
								CollectionFactory $productCollectionFactory)
	{
		$this->_productCollectionFactory = $productCollectionFactory;
		$this->_resultJsonFactory = $resultJsonFactory;
		parent::__construct($context);
	}

	public function execute()
	{
		$resultJson = $this->_resultJsonFactory->create();

		$params = $this->getRequest()->getParams();
		$textSearch = isset($params['q']) ? $params['q'] : '';
		$response = null;

		if ('' !== $textSearch){
			try{
				$productCollection = $this->_objectManager
					->create('Magento\Catalog\Model\ResourceModel\Product\Collection')
					->addAttributeToSelect([
						'name',
						'image',
						'url_key',
						'visibility'
					])
					->addAttributeToFilter('name', array(
						'like' => '%'.$textSearch.'%'
					))
					->addAttributeToFilter('visibility', array(
						'in' => array(2,3,4)
					))
					->setPageSize(5);
				;


				$productResult = [];
				foreach ($productCollection as $product) {
					$temp = array(
						'name' => $product['name'],
						'image' => $product['image'],
						'url_key' => $product['url_key'],
						'visibility' => $product['visibility']
					);
					array_push($productResult, $temp);

				}
				$response['product'] = $productResult;
			}
			catch (\Exception $e){
				$response['message'] = $e->getMessage();
			}
		}
		return $resultJson->setData($response);
	}
}